package ro.pub.cs.systems.eim.practicaltest02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.ServerSocket;

public class MainActivity extends AppCompatActivity {

    TextView responseView;
    EditText urlText, port;
    Button go, start;
    ServerThread serverThread = null;
    ServerSocket serverSocket = null;
    ClientThread clientThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test02_main);
        responseView = findViewById(R.id.response);
        urlText = findViewById(R.id.urlText);
        port = findViewById(R.id.port);
        go = findViewById(R.id.go);
        start = findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serverPort = port.getText().toString();
                if (serverPort == null || serverPort.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "[MAIN] Server port is missing", Toast.LENGTH_SHORT).show();
                    return;
                }
                serverThread = new ServerThread(Integer.parseInt(serverPort));
                if (serverThread.getServerSocket() == null) {
                    Log.e("tag", "[MAIN] Could not create server thread!");
                    return;
                }
                serverThread.start();
            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clientAddress = "127.0.0.1";
                String clientPort = port.getText().toString();
                if (clientAddress == null || clientAddress.isEmpty()
                        || clientPort == null || clientPort.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "[MAIN] Client connection parameters should be filled!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (serverThread == null || !serverThread.isAlive()) {
                    Toast.makeText(getApplicationContext(), "[MAIN] There is no server running!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String url = urlText.getText().toString();
                if (url == null || url.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "[MAIN] Url should be filled", Toast.LENGTH_SHORT).show();
                    return;
                }
                responseView.setText("");
                clientThread = new ClientThread(url, clientAddress, Integer.parseInt(clientPort), responseView);
                clientThread.start();
            }
        });
    }

    @Override
    protected void onDestroy() {
        Log.i("tag", "[MAIN] onDestroy() callback method has been invoked");
        if (serverThread != null) {
            serverThread.stopThread();
        }
        super.onDestroy();
    }
}
