package ro.pub.cs.systems.eim.practicaltest02;

import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.client.ClientProtocolException;

public class ServerThread extends Thread{
    ServerSocket serverSocket = null;
    ArrayList<String> firewall = new ArrayList<>();
    public ServerThread(Integer port) {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        firewall.add("http://google.com");
    }
    public ServerSocket getServerSocket() {
        return this.serverSocket;
    }
    public synchronized ArrayList<String> getFirewall() {
        return firewall;
    }
    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Log.i("tag", "[SERVER] Waiting for a client invocation...");
                Socket socket = serverSocket.accept();
                Log.i("tag", "[SERVER] A connection request was received from " + socket.getInetAddress() + ":" + socket.getLocalPort());
                CommunicationThread communicationThread = new CommunicationThread(this, socket);
                communicationThread.start();
            }
        } catch (ClientProtocolException clientProtocolException) {
            Log.e("tag", "[SERVER] An exception has occurred: " + clientProtocolException.getMessage());
            clientProtocolException.printStackTrace();
        } catch (IOException ioException) {
            Log.e("tag", "[SERVER] An exception has occurred: " + ioException.getMessage());
            ioException.printStackTrace();
        }
    }

    public void stopThread() {
        interrupt();
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException ioException) {
                Log.e("tag", "[SERVER] An exception has occurred: " + ioException.getMessage());
                ioException.printStackTrace();
            }
        }
    }
}


