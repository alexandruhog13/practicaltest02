package ro.pub.cs.systems.eim.practicaltest02;

import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientThread extends Thread {
     String url;
    TextView responseView;
    int port;
    String address;

    Socket socket = null;

    public ClientThread(String url, String address, int port, TextView responseView) {
        this.url = url;
        this.responseView = responseView;
        this.port = port;
        this.address = address;
    }
    @Override
    public void run() {
        try {
            String content;
            socket = new Socket(address, port);
            if (socket == null) {
                Log.e("tag", "[CLIENT] Socket creation err!");
                return;
            }
            BufferedReader bufferedReader = Utilities.getReader(socket);
            PrintWriter printWriter = Utilities.getWriter(socket);
            if (bufferedReader == null || printWriter == null) {
                Log.e("tag", "[CLIENT] Buffered Reader/ Print Writer are null!");
                return;
            }
            printWriter.println(url);
            while ((content = bufferedReader.readLine()) != null) {
                final String finalizedContent = content;
                responseView.post(new Runnable() {
                    @Override
                    public void run() {
                        responseView.setText(finalizedContent);
                    }
                });
            }
        } catch (IOException ioException) {
            Log.e("tag", "[CLIENT] An exception has occurred: " + ioException.getMessage());
            ioException.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ioException) {
                    Log.e("tag", "[CLIENT] An exception has occurred: " + ioException.getMessage());
                    ioException.printStackTrace();

                }
            }
        }
    }
}
