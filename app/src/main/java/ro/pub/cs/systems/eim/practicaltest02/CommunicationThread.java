package ro.pub.cs.systems.eim.practicaltest02;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

class CommunicationThread extends Thread {
    Socket socket = null;
    ServerThread serverThread = null;

    public CommunicationThread(ServerThread serverThread, Socket socket) {
        this.socket = socket;
        this.serverThread = serverThread;

    }

    public void run() {
        if (socket == null) {
            Log.e("tag", "[COMMUNICATION] Socket can't be null!");
            return;
        }
        try {
            BufferedReader bufferedReader = Utilities.getReader(socket);
            PrintWriter printWriter = Utilities.getWriter(socket);
            if (bufferedReader == null || printWriter == null) {
                Log.e("tag", "[COMMUNICATION] Buffered Reader/ Print Writer are null!");
                return;
            }
            Log.i("tag", "[COMMUNICATION] Waiting for url");
            String url = bufferedReader.readLine();
            if (url == null || url.isEmpty()) {
                Log.e("tag", "[COMMUNICATION] Error receiving url from client");
                return;
            }
            ArrayList<String> firewall = serverThread.getFirewall();

            if (firewall.contains(url)){
                Log.e("tag", "[COMMUNICATION] URL Allowed!");
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                String content = httpClient.execute(httpGet, responseHandler);
                printWriter.println(content);
            } else {
                Log.e("tag", "[COMMUNICATION] URL Blocked!");
                printWriter.println("Not allowed!");
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ioException) {
                    Log.e("tag", "[COMMUNICATION] An exception has occurred: " + ioException.getMessage());
                    ioException.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
